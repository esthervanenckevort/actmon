# makefile
# Created by IBM WorkFrame/2 MakeMake at 23:40:30 on 20 Sept 2000
#
# The actions included in this make file are:
#  Compile::Resource Compiler
#  Compile::C++ Compiler
#  Link::Linker
#  Bind::Resource Bind

.SUFFIXES:

.SUFFIXES: \
    .c .obj .rc .res 

.rc.res:
    @echo " Compile::Resource Compiler "
    rc.exe -r %s %|fF.RES

{.}.rc.res:
    @echo " Compile::Resource Compiler "
    rc.exe -r %s %|fF.RES

.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Tdp /O /Gm /Gt /C %s

{.}.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Tdp /O /Gm /Gt /C %s

all: \
    .\actmon.exe

.\actmon.exe: \
    .\helper.obj \
    .\properties.obj \
    .\actmon.obj \
    .\actmon.res \
    {$(LIB)}os2386.lib \
    {$(LIB)}cppom30.lib \
    dll\actmon.lib \
    {$(LIB)}actmon.def
    @echo " Link::Linker "
    @echo " Bind::Resource Bind "
    icc.exe @<<
     /B" /packd /optfunc"
     /Feactmon.exe 
     os2386.lib 
     cppom30.lib 
     dll\actmon.lib 
     actmon.def
     .\helper.obj
     .\properties.obj
     .\actmon.obj
<<
    rc.exe .\actmon.res actmon.exe

.\actmon.res: \
    .\actmon.rc \
    .\RedN.ICO \
    .\RedO.ICO \
    .\RedM.ICO \
    .\RedT.ICO \
    .\RedC.ICO \
    .\RedA.ICO \
    .\YellowN.ICO \
    .\YellowO.ICO \
    .\YellowM.ICO \
    .\YellowT.ICO \
    .\YellowC.ICO \
    .\YellowA.ICO \
    .\GreenN.ICO \
    .\GreenO.ICO \
    .\GreenM.ICO \
    .\GreenT.ICO \
    .\GreenC.ICO \
    .\GreenA.ICO \
    .\actmon.ico \
    .\dialog.dlg \
    .\dialog.h \
    .\actmon.h

.\helper.obj: \
    .\helper.c \
    dll\actdll.h \
    .\actmon.h \
    .\helper.h \
    .\properties.h \
    .\dialog.h

.\actmon.obj: \
    .\actmon.c \
    dll\actdll.h \
    .\actmon.h \
    .\helper.h \
    .\properties.h \
    .\dialog.h

.\properties.obj: \
    .\properties.c \
    dll\actdll.h \
    .\actmon.h \
    .\helper.h \
    .\properties.h \
    .\dialog.h
